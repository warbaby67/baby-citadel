<?

/**
 * BabySocketStream - A wrapper for stream_socket_client()
 * @author Warren Stevens (warbaby67@gmail.com)
 * @package baby-citadel
 **/

Abstract Class BabySocketStream
	{

	protected $path_to_socket = null;

	protected $stream = false;

	protected $term = "\n";

	protected $sleep_seconds = 0;

	# Fool with this value if you get timeouts.
	protected $sleep_nanoseconds = 90000;

	public $raw = null;

	function __construct()
		{
		$this->stream = stream_socket_client('unix://'.$this->path_to_socket, $error, $string);
		if ($error)
			{
			die('I fail because '. $string);
			}
		else
			{
			$this->read();
			}
		}

	function close() { return fclose($this->stream); }

	function read($length = 6096) {  $this->raw = fread($this->stream, $length); }

	function send($string = null)
		{
		fwrite($this->stream, $string.$this->term);
		@time_nanosleep($this->sleep_seconds, $this->sleep_nanoseconds);
		$this->read();
		}

	function meta_data() { return stream_get_meta_data($this->stream); }

	function unread_bytes()
		{
		$meta = $this->meta_data();
		return $meta['unread_bytes'];
		}

	}
