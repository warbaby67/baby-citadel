<?

/**
 * BabyCitadelServerResponseArray - Several responses are version specific.  No attempt has been made to deal with that here.
 * @author Warren Stevens (warbaby67@gmail.com)
 * @package baby-citadel
 **/

class BabyCitadelServerResponseArray
	{

	public static $keys = array(
			'rwho' =>  array(
         			 0 => 'session_id', # Session ID. Citadel fills this with the pid of a server program.
         			 1 => 'user_name',  # User name.
         			 2 => 'room',       # The name of the room the user is currently in. This field might not be displayed (for example, if the user is in a private room) or it might contain other information (such as the name of a file the user is downloading).
         			 3 => 'host',       # (server v4.03 and above) The name of the host the client is connecting from, or .localhost. if the client is local.
         			 4 => 'client',     # (server v4.04 and above) Description of the client software being used
         			 5 => 'last_time',  # The last time, locally to the server, that a command was received from this client (Note: NOOP's don't count)
         			 6 => 'last_cmd',   # The last command received from a client. (NOOP's don't count)
         			 7 => 'flags',      # Session flags. These are: + (spoofed address), - (STEALTH mode), * (posting) and . (idle).
         			 8 => 'real_name',  # Actual user name, if user name is masqueraded and viewer is an Aide.
         			 9 => 'real_room',  # Actual room name, if room name is masqueraded and viewer is an Aide.
        			10 => 'real_host',  # Actual host name, if host name is masqueraded and viewer is an Aide.
        			11 => 'logged_in'), # Nonzero if the session is a logged-in user, zero otherwise.
			'info' => array(
					 0 => 'session_id',	# Your unique session ID on the server
					 1 => 'node_name',	# The node name of the Citadel server
					 2 => 'node_human', # Human-readable node name of the Citadel server
					 3 => 'fqdn', 		# The fully-qualified domain name (FQDN) of the server
					 4 => 'server', 	# The name of the server software
					 5 => 'version', 	# (The revision level of the server code) * 100
					 6 => 'location',	# The geographical location of the site (city and state if in the US)
					 7 => 'sysadmin', 	# The name of the system administrator
					 # 8 => 'server_type',# A number identifying the server type (see below)
					 8 => 'paginator',	# The text of the system's paginator prompt
					 9 => 'floor_flag',	# Floor Flag. 1 if the system supports floors, 0 if otherwise.
					10 => 'paging_level',# Paging level. 
					11 => 'unused',		# (blank - no longer in use)
					12 => 'qnop',		# Set to nonzero if this server supports the QNOP command.
					13 => 'ldap',		# Set to nonzero if this server is capable of connecting to a directory service using LDAP.
					#14 => 'self_serve',	# Set to nonzero if this server does not allow self-service creation of new user accounts.
					14 => 'timezone',	# The default timezone for calendar items which do not have any timezone specified and are not flagged as UTC. This will be a zone name from the  'Olsen database.
					15 => 'load_average',# LoadAverage; Average Load calculation inside of citadel. *1
					16 => 'worker_average',# WorkerAverage; average count of workerthreads *1
					17 => 'thread_count',# ThreadCount; Actual number of worker threads *1
					18 => 'sieve',		# Nonzero if the server supports filtering of incoming mail using the Sieve language.
					#20 => 'full_text',	# Nonzero if the server's full text index is enabled.
					19 => 'svn_repos',	# The servers SVN repository revision code.
					# 23 => 'open_id_ver',# Version of OpenID supported by the server for authentication. 0 if no support.
					20 => 'allow_guest'), # Nonzero if the server supports anonymous guest logins

			'list' => array(
					 0 => 'display_name', # User display name
					 1 => 'access_level', # Access level
					 2 => 'user_number',  # User number
					 3 => 'last_login',   # Date/time of last login (Unix timestamp format)
					 4 => 'lotal_logins', # Total number of logins
					 5 => 'total_msgs',   # Total number of messages posted
					 6 => 'password',	  # Password (listed only if the user requesting the list is an Aide
					 7 => 'unknown')	  # No clue what this is.
			);
	}
