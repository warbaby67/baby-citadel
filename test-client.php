#!/usr/bin/php
<?

require_once('init.php');

$admin_user = 'admin';
$admin_pass = 'fubar';

 /**
  * In order to use this package, you need a Citadel server running locally.
  * Try http://www.citadel.org/doku.php/doku.php?id=installation:start
  **/

# Check out the server.  If you have a problem here
# make sure BabyCitadelServer::$path_to_socket is 
# right for your operating system.

$server = new BabyCitadelServer();

# Attempt a login

if ($server->login($admin_user, $admin_pass))
	{
	print "\nServer Time\n";
	print $server->time();

	print "\nSend an IM to yourself\n";
	print $server->im($admin_user, 'What is up with that?');
	print "\nDisable IM\n";
	print $server->disable_im();
	print "\nEnable IM\n";
	print $server->enable_im();
	print "\nUsers on this system\n";
	foreach ($server->users() as $who)
		{
		print_R($who);
#		$string = implode(":", $who);
#		print $string."\n";
		}
	print "\nUsers online\n";
	print_R($server->who_is_online());
	}
else
	{
	die($server->response->full_status());
	}
