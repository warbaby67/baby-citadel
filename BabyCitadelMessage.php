<?

/**
 * Class BabyCitadelMessage
 * @author Warren Stevens (warbaby67@gmail.com)
 * @package baby-citadel
 *
 * 	order	name			desc
 *
 *	  0 	post			Post flag. This should be set to 1 to post a message. If it is set to 0, the server only returns OK or ERROR (plus any flags describing the error) without reading in a message. Client software should, in fact, perform this operation at the beginning of an �enter message� command�before�starting up its editor, so the user does not end up typing a message in vain that will not be permitted to be saved.
 *	  1 	recp			Recipient (To: field). This argument is utilized only for private mail. It is ignored for public messages. It contains, of course, the name of the recipient(s) of the message.
 *	  2		anon_flag		Anonymous flag. This argument is ignored unless the room allows anonymous messages. In such rooms, this flag may be set to 1 to flag a message as anonymous, otherwise 0 for a normal message.
 *	  3		format_type		Format type. Any valid Citadel format type may be used (this will typically be 0)
 *	  4		subject			Subject. If present, this argument will be used as the subject of the message.
 *	  5		newusername		Post name. This is the 'display name' or 'screen name' to be used as the author of the message. It is permissible to leave this field blank to allow the server to select a suitable default. The supplied name must be one of the names returned by a GVSN command, unless the user is an Aide.",
 *	  6		do_confirm		Do Confirmation. NOTE: this changes the protocol semantics! When you set this to nonzero, ENT0 will reply with a confirmation message after you submit the message text. The reply code for the ENT0 command will be START_CHAT_MODE instead of SEND_LISTING.
 *	  7		cc				Recipient (Cc: field). This argument is utilized only for private mail. It is ignored for public messages. It contains, of course, the name of the recipient(s) of the message.
 *	  8		bcc				Recipient (Bcc: field). This argument is utilized only for private mail. It is ignored for public messages. It contains, of course, the name of the recipient(s) of the message.
 *	  9		supplied_euid	Exclusive message ID. When a message is submitted with an Exclusive message ID, any existing messages with the same ID will automatically be deleted. This is only applicable for Wiki rooms; other types of rooms either ignore the supplied ID (such as message boards and mailboxes) or derive the ID from a UUID native to the objects stored in them (such as calendars and address books).
 *	 10		newusermail		Email address. This is the Internet email address to be used as the author of the message. It is permissible to leave this field blank to allow the server to select a suitable default. The supplied name must be one of the names returned by a GVEA command.
 *	 11		references		If this is a reply to another message, supply a delimiter-separated list of message ID's for the thread, starting with the message being replied to, and ending with the thread root. Note that the delimiter is �!� (exclamation point) rather than the vertical bar, because we are already using the vertical bar delimiter here.
**/

class BabyCitadelMessage
	{


	public $id = false;

	private $me = array();

	public $fields = array('post', 'recp', 'anon_flag', 'format_type', 'subject', 'newusername', 'do_confirm', 'cc', 'bcc', 'supplied_euid', 'newuseremail', 'references');

	public static $formats = array(
            0 => 'FMT_CITADEL', # Traditional. Citadel formatting. This means that newlines should be treated as spaces UNLESS the first character on the next line is This allows a message to be formatted to the reader's screen width. It also allows the use of proportional fonts.
            1 => 'FMT_FIXED',   # A simple fixed-format message. The message should be displayed to the user's screen as is, preferably in a fixed-width font that will fit 80 columns on a screen.
            4 => 'FMT_RFC822'); # MIME format message. The message text is expected to contain a header with the Content-type: directive (and possibly others).

    /**
     * @param array $a passed to set
     */
    function __construct(array $a)
		{
		# Init values for me
		foreach ($this->fields as $field)
			{
			$a[$field] = (empty($a[$field]) ? null : $a[$field]);
			}
		$this->set($a); 
		}

    function set(array $a)
        {
        foreach ($a as $k => $v)
        	{
            if (in_array($k, $this->fields))
            	{
                $this->me[$k] = $v;
                }
            }
        }

    function __call($key, $args) { return $this->me[$key]; }


##### PUBLIC 

	function get() { return $this->me; }

	function delimited() { return implode('|', $this->me); }

##### PUBLIC STATIC

	public static function factory_post($a)
		{

		}

	public static function factory($a)
		{

		}

	}
