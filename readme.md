Baby Citadel
======
----

Baby Citadel is set of PHP 5x Classes for use in your web or cli scripts. With this package you can easily make a RESTful API to Citadel. 

To fully understand how Citadel Groupware works through the API is complex. It is important you read everything at http://citadel.org/doku.php?id=documentation:start 

Make sure you drill down through all the text.

If you need more help with Citadel, make an account for yourself at [http://uncensored.citadel.org/] where you can ask questions. 

Remember that Uncensored! BBS is not a support forum for this software. So be a mensch and don't bother them with issues readily explained in the documentation.


Installation
=====

  * Install Citadel.
  * Install PHP 5.x  - Make sure you have the sockets extension installed & enabled.
  * Update BabyCitadelServer::$path_to_socket with the appropriate path to the socket file.
  * Edit client.php to include YOUR $admin_user and $admin_pass
  * Run client.php from the command line: php client.php

If you get lost, read the example controller client.php and check the methods in BabyCitadelServer.php

Contribute
====

Per Ignatius T Foobar, these are the official PHP bindings to Citadel, so if you are using this package please consider contributing. 

  * There are many more methods to be written and tested. 

  * It would be really nice if someone could make direct links to the pertinent docuwiki pages at citadel.org.  I have to look them up every time, and they are buried.

  * To the developer in Germany, I lost your email address in an IMAP accident. Please contact me.

  * robotamer at github, I don't recall talking to you & I'm not sure why you created the repository at https://github.com/robotamer/Citadel-PHP  If you want to fork, that's fine, but at least have the decency to call it a fork.  I didn't write this for your resume, buddy-boy.

  * I see a lot of downloads, but nobody is committing. I know the tarball is convenient but if you want to be a man, check it out through subversion. Think about the community that has given you so much free software, until the guilt wells up inside and you break down in tears of repentance.  

  * Send me an email if you need a license to distribute Baby Citadel with proprietary code, or if you are in Nigeria and want to send me some checks.

Github
======

No. Github has an unreasonable and arbitrary suspension policy which hurts developers, and they have gone insane. I can barely search the bloody site without getting my IP blocked. 

BitBucket
======
Hey, we moved to bitbucket instead!  Please be patient while I decruft the markdown.

License
======
Creative Commons  http://creativecommons.org/licenses/by-nc/3.0/

Acknowledgements
=====

Thanks to 

  * Steve Blass, who introduced me to Citadel in 1992. Thanks Steve! http://www.networkworld.com/columnists/blass.html 

 * Ignatius T Foobar, current Citadel maintainer and a true unsung hero of Open Source development https://www.google.com/search?q=Ignatius+T+Foobar 


Regards,

-Warren Steven (warbaby67@gmail.com)

----
Last Updated Nov 21st, 2013
