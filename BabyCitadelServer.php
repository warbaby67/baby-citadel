<?

/**
 * BabyCitadelServer - Extension of BabySocketStream
 * @author Warren Stevens (warbaby67@gmail.com)
 * @package baby-citadel
 **/

class BabyCitadelServer extends BabySocketStream

	{

	/**
	 * Full path to citadel server socket file.
	 * @var string $path_to_socket
	 * @access public
	 **/

	public $path_to_socket = '/usr/local/citadel/citadel.socket';

	public $response = null; # BabyCitadelServerResponse Object

	function __construct() { return parent::__construct(); }

##### PUBLIC


	public function enable_im()
		{
		$this->send('DEXP 1');
		return ($this->response->status_code() == '200' ? true : false);
		}

	public function disable_im()
		{
		$this->send('DEXP 0');
		return ($this->response->status_code() == '200' ? true : false);
		}

	/**
	 * Developer/Version dependent, left here for others
	 * @return string
	 **/
	public function identify_client()
		{
		return 'not implimented';
		#$this->send('IDEN blah blah blah');
		#return $this->response->first_line_array();
		}

	/**
	 * @return array
	 **/
	public function info()
		{
		$this->send('INFO');
		return $this->response->lines_as_array_of('info');
		}


	public function im($user, $message)
		{
		$this->send('SEXP '.$user. '|'.$message);
		return $this->response->full_status();
		}

	public function login($user, $pass)
		{
		$this->send('USER '.$user);
		if ($this->response->status_code() != '300') # wants more data
			{
			return false;
			}
		else
			{
			$this->send('PASS '.$pass);
			if ($this->response->status_code() != '200') # ok
				{
				return false;
				}
			else
				{
				return true;
				}
			}
		}

	/**
	 * logout() always returns OK even if no-one is logged in
	 * @param
	 * @return void
	 **/
	public function logout() { $this->send('LOUT');}


	public function post($recipient)
		{
/*
		$params = array();
		$params['post'] 	 = '1';
		$params['recipient'] = $recipient;
		$params['anon_flag'] = null;
		$params['format_type'] = 
		$this->send('POST 1 '.$recipient

ENT0 1|Kern LETSGeel <kern-wE+tr93vHrbAJx/1eAfhutaaRNfwGcAC@xxxxxxxxxxxxxxxx>|0|4| LETSGeel: Marva accounts voor andere
 LETS groepen|Guy Van Sanden|||||gvs@xxxxxxxxx

ENT0 $post_flag,  
 ENT0 (ENTer message, mode 0)

This command is used to enter messages into the system. It accepts four arguments:

No.	Name	Value
0	post	Post flag. This should be set to 1 to post a message. If it is set to 0, the server only returns OK or ERROR (plus any flags describing the error) without reading in a message. Client software should, in fact, perform this operation at the beginning of an .enter message. command before starting up its editor, so the user does not end up typing a message in vain that will not be permitted to be saved.
1	recp	Recipient (To: field). This argument is utilized only for private mail. It is ignored for public messages. It contains, of course, the name of the recipient(s) of the message.
2	anon_flag	Anonymous flag. This argument is ignored unless the room allows anonymous messages. In such rooms, this flag may be set to 1 to flag a message as anonymous, otherwise 0 for a normal message.
3	format_type	Format type. Any valid Citadel format type may be used (this will typically be 0; see the MSG0 command below).
4	subject	Subject. If present, this argument will be used as the subject of the message.
5	newusername	Post name. This is the 'display name' or 'screen name' to be used as the author of the message. It is permissible to leave this field blank to allow the server to select a suitable default. The supplied name must be one of the names returned by a GVSN command, unless the user is an Aide.
6	do_confirm	Do Confirmation. NOTE: this changes the protocol semantics! When you set this to nonzero, ENT0 will reply with a confirmation message after you submit the message text. The reply code for the ENT0 command will be START_CHAT_MODE instead of SEND_LISTING.
7	cc	Recipient (Cc: field). This argument is utilized only for private mail. It is ignored for public messages. It contains, of course, the name of the recipient(s) of the message.
8	bcc	Recipient (Bcc: field). This argument is utilized only for private mail. It is ignored for public messages. It contains, of course, the name of the recipient(s) of the message.
9	supplied_euid	Exclusive message ID. When a message is submitted with an Exclusive message ID, any existing messages with the same ID will automatically be deleted. This is only applicable for Wiki rooms; other types of rooms either ignore the supplied ID (such as message boards and mailboxes) or derive the ID from a UUID native to the objects stored in them (such as calendars and address books).
10	newusermail	Email address. This is the Internet email address to be used as the author of the message. It is permissible to leave this field blank to allow the server to select a suitable default. The supplied name must be one of the names returned by a GVEA command.
11	references	If this is a reply to another message, supply a delimiter-separated list of message ID's for the thread, starting with the message being replied to, and ending with the thread root. Note that the delimiter is .!. (exclamation point) rather than the vertical bar, because we are already using the vertical bar delimiter here.
*/
		}

	/**
	 * Terminates your connection
	 * @param none
	 **/
	public function quit() { $this->send('QUIT'); }

	public function terminate($session_id)
		{
		$this->send('TERM '.$session_id);
		return $this->response->full_status();
		}
	/**
	 * @return string mysql datetime format
	 **/
	public function time()
		{
		$this->send('TIME');
		$junk = $this->response->first_line_array();
		return date("Y-m-d h:i:s", $junk[0]);
		}

	/**
	 * @return array
	 **/
	public function who_is_online()
		{
		$this->send('RWHO');
		return $this->response->lines_as_delimited_array_of('rwho');
		}

	public function users($include_system_users = false)
		{
		$this->send('LIST');
		$all_users = $this->response->lines_as_delimited_array_of('list');
		if ($include_system_users == true)
			{
			return $all_users;
			}
		else
			{
			$users = array();
			foreach ($all_users as $user)
				{
				if (substr($user['display_name'], 0, 3) != 'SYS')
					{
					$users[] = $user;
					}
				}
			return $users;
			}
		}

##### PROTECTED


##### OVERRIDES

	public function send($string)
		{
		parent::send($string);
		$parts = explode(" ", $string);
		$cmd = strtolower($parts[0]); # $keys relies on this
		$this->response = new BabyCitadelServerResponse($this->raw, $cmd);
		}

##### PRIVATE

	}
