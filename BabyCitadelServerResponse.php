<?

/**
 * BabyCitadelServerResponse
 * @author Warren Stevens (warbaby67@gmail.com)
 * @package baby-citadel
 **/

class BabyCitadelServerResponse
	{

    /**
     * Togther with the suffix, explains a status code.
     * @var array $status_code_prefix
     * @access public
     * @link http://www.citadel.org/doku.php/documentation:appproto:statuscodes
	 * @link http://www.citadel.org/doku.php/documentation:appproto:app_proto#resultcodes
     **/
    public static $status_code_prefix = array(
        '1' => 'LISTING_FOLLOWS',   # The requested operation is progressing and is now delivering text.The client *must* now read lines of text until it receives thetermination sequence (?000? on a line by itself).
        '2' => 'CIT_OK',            # The requested operation succeeded.
        '3' => 'MORE_DATA',         # The requested operation succeeded so far', but another command is required to complete it.
        '4' => 'SEND_LISTING',      # The requested operation is progressing and is now expecting text. The client *must* now transmit zero or more lines of text followed by the termination sequence (?000? on a line by itself).
        '5' => 'ERROR',             # The requested operation failed. The second and third digits of the error code and/or the error message following it describes why.
        '6' => 'BINARY_FOLLOWS',    # After this line please read n bytes. (n follows after a blank)
        '7' => 'SEND_BINARY',       # You now may send us n bytes binary data. (n follows after a blank)
        '8' => 'START_CHAT_MODE',   # Ok, we are in chat mode now. every line you send will be broadcasted.
        '9' => 'ASYNC_MSG');        # There is a page waiting for you, please fetch it.

    /**
     * Extended description of status code
     * @var array $status_code_suffix
     * @access public
     * @link http://www.citadel.org/doku.php/documentation:appproto:statuscodes
	 * @link http://www.citadel.org/doku.php/documentation:appproto:app_proto#resultcodes
     **/
    public static $status_code_suffix = array(
        '02' => 'ASYNC_GEXP',
        '10' => 'INTERNAL_ERROR',
        '11' => 'TOO_BIG',
        '12' => 'ILLEGAL_VALUE',
        '20' => 'NOT_LOGGED_IN',
        '30' => 'CMD_NOT_SUPPORTED',
        '31' => 'SERVER_SHUTTING_DOWN',
        '40' => 'PASSWORD_REQUIRED',
        '41' => 'ALREADY_LOGGED_IN',
        '42' => 'USERNAME_REQUIRED',
        '50' => 'HIGHER_ACCESS_REQUIRED',
        '51' => 'MAX_SESSIONS_EXCEEDED',
        '52' => 'RESOURCE_BUSY',
        '53' => 'RESOURCE_NOT_OPEN',
        '60' => 'NOT_HERE',
        '61' => 'INVALID_FLOOR_OPERATION',
        '70' => 'NO_SUCH_USER',
        '71' => 'FILE_NOT_FOUND',
        '72' => 'ROOM_NOT_FOUND',
        '73' => 'NO_SUCH_SYSTEM',
        '74' => 'ALREADY_EXISTS',
        '75' => 'MESSAGE_NOT_FOUND');

	private $me = array();

	public $fields = array('all_lines', 'cmd', 'first_line_data', 'lines', 'status_code');

	function __construct($response_body, $cmd)
		{
		$this->me['all_lines'] = explode("\n", $response_body);
		$this->me['cmd'] = $cmd;
		$this->me['first_line_data'] = self::get_first_line_data($this->me['all_lines'][0]);
		$this->me['status_code'] = self::get_status_code($this->me['all_lines'][0]);
		$lines = $this->all_lines();
		# Now, lets remove the first line
		unset($lines[0]);
		# Iterate through ignoring the blank last
	 	# line and the end of list line '000'.
		for ($c = 1; $c <= count($lines)-1; $c++)
			{
			if ($lines[$c] != '000')
				{
				$this->me['lines'][] = $lines[$c];
				}
			}
		}

	function __call($k, $args = array()) { return $this->me[$k]; }


	public function lines_as_array_of($key)
		{
		$keys = self::get_keys($key);
		return self::relabel_keys($this->lines(), $keys);
		}

	public function lines_as_delimited_array_of($key)
		{
		$keys = self::get_keys($key);
		$return = array();
		foreach ($this->lines() as $line)
			{
			$return[] = self::relabel_keys(explode('|', $line), $keys);
			}
		return $return;
		}

	public function first_line_array() { return $this->first_line_data_as_array(); }

	public function first_line_data_as_array() { return explode('|', $this->first_line_data());}

	public function full_status() { return $this->status_code().':'.$this->status_code_text(); }

	public function status_code_text()
		{
		if (substr($this->status_code(), 1,2) == '00')
			{
			return self::$status_code_prefix[substr($this->status_code(), 0, 1)];
			}
		else
			{
			return self::$status_code_prefix[substr($this->status_code(), 0, 1)].':'.self::$status_code_suffix[substr($this->status_code(), 1,2)];
			}
		}

##### PROTECTED STATIC 

	# keys posn has to match
	protected static function relabel_keys(array $data, array $keys)
		{
		if (count($data) != count($keys))
			{
			print('key count: '.count($keys).', data count: '.count($data)); 
			}
		$return = array();
		foreach ($keys as $key_id => $key_label)
			{
			$return[$key_label] = $data[$key_id];
			}
		return $return;
		}

	protected static function get_status_code($string)	{ return substr($string, 0, 3); }

	protected static function get_first_line_data($string)	{ return substr($string, 4, strlen($string)); }

	protected static function get_keys($key)
		{
		require_once('BabyCitadelServerResponseArray.php');
		return BabyCitadelServerResponseArray::$keys[$key];
		}
	}
